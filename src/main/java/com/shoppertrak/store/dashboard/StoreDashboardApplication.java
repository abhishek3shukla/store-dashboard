package com.shoppertrak.store.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreDashboardApplication.class, args);
	}

}
