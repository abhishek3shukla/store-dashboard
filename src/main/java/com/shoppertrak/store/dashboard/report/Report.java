package com.shoppertrak.store.dashboard.report;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author Abhishek Shukla
 */
@Entity
@Data
@Table(name = "report")
public class Report implements Serializable {

	private static final long serialVersionUID = 7501236854841404284L;
	
	@NotNull
	@Column(name = "date")
	private LocalDate date;
	
	@NotNull
	@Column(name = "action")
	private String action;
	
	@Id
	@Column(name = "report_id")
	private int id;
	
	@NotNull
	@Column(name = "department")
	private String department;
	
	@NotNull
	@Column(name = "report_name")
	private String name;

}