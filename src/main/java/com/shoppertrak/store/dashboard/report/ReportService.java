package com.shoppertrak.store.dashboard.report;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppertrak.store.dashboard.repository.ReportRepository;

@Service
public class ReportService {
	
	private ReportRepository repository;
	
	@Autowired
	public ReportService(ReportRepository repository) {
		this.repository = repository;
	}
	
	public List<Report> getReportDetails() {
		return repository.findAll();
	}

}

