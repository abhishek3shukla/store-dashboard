package com.shoppertrak.store.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppertrak.store.dashboard.report.Report;
import com.shoppertrak.store.dashboard.report.ReportService;

@RestController
public class ReportController {

	@Autowired
	private ReportService service;

	@GetMapping("/report")
	public List<Report> getReportDetails() {
		return service.getReportDetails();
	}

}
