package com.shoppertrak.store.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppertrak.store.dashboard.inventory.Inventory;
import com.shoppertrak.store.dashboard.inventory.InventoryService;


@RestController
public class InventoryController {
	
	@Autowired
	private InventoryService service;
	
	@GetMapping("/inventory")
	public List<Inventory> getInventoryDetails() {
		return service.getInventoryDetails();
	}

}
