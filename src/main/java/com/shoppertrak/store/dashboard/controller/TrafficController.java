package com.shoppertrak.store.dashboard.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shoppertrak.store.dashboard.traffic.TrafficService;
import com.shoppertrak.store.dashboard.traffic.TrafficTuple;

@RestController
public class TrafficController {
	
	@Autowired
	private TrafficService service;
	
	@GetMapping("/traffic")
	public Map<Integer, List<TrafficTuple>> getTrafficDetails() {
		return service.getTrafficDetails();
	}

}
