package com.shoppertrak.store.dashboard.traffic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppertrak.store.dashboard.repository.TrafficRepository;

@Service
public class TrafficService {
	
	private TrafficRepository repository;
	
	@Autowired
	public TrafficService(TrafficRepository repository) {
		this.repository = repository;
	}
	
	public Map<Integer, List<TrafficTuple>> getTrafficDetails() {
		List<Traffic> result =  repository.findAll();
		
		Map<Integer, List<TrafficTuple>> map = new HashMap<Integer, List<TrafficTuple>>();
		List<TrafficTuple> tlist1 = new ArrayList<TrafficTuple>();
		List<TrafficTuple> tlist2 = new ArrayList<TrafficTuple>();
		List<TrafficTuple> tlist3 = new ArrayList<TrafficTuple>();
		TrafficTuple tuple;
		for(Traffic traffic: result) {

			if(traffic.getFloor() == 1) {
				tuple = new TrafficTuple();
				tuple.setZoneName(traffic.getZone_name());
				tuple.setCount(traffic.getCount());
				tlist1.add(tuple);				
			}
			if(traffic.getFloor() == 2) {
				tuple = new TrafficTuple();
				tuple.setZoneName(traffic.getZone_name());
				tuple.setCount(traffic.getCount());
				tlist2.add(tuple);
			}
			if(traffic.getFloor() == 3) {
				tuple = new TrafficTuple();;
				tuple.setZoneName(traffic.getZone_name());
				tuple.setCount(traffic.getCount());
				tlist3.add(tuple);
			}
		}
		map.put(1, tlist1);
		map.put(2, tlist2);
		map.put(3, tlist3);
		return map;
	}

}
