package com.shoppertrak.store.dashboard.traffic;

import java.io.Serializable;

import lombok.Data;

@Data
public class TrafficTuple implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String zoneName;
	private long count;

}
