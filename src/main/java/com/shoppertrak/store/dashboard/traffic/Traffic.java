package com.shoppertrak.store.dashboard.traffic;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author Abhishek Shukla
 */
@Entity
@Data
@Table(name = "traffic")
public class Traffic implements Serializable {

	private static final long serialVersionUID = 7501236854841404284L;
	
	@NotNull
	@Column(name = "date")
	private LocalDate date;
	
	@NotNull
	@Column(name = "count")
	private long count;
	
	@Id
	@Column(name = "zone_name")
	private String zone_name;
	
	@NotNull
	@Column(name = "floor")
	private int floor;

}
