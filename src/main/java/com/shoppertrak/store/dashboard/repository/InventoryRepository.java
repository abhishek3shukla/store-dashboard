package com.shoppertrak.store.dashboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoppertrak.store.dashboard.inventory.Inventory;

/**
 * @author Abhishek Shukla
 */
@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {
	
	List<Inventory> findAll();

}

