package com.shoppertrak.store.dashboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shoppertrak.store.dashboard.traffic.Traffic;

@Repository
public interface TrafficRepository extends JpaRepository<Traffic, Long> {
	
	List<Traffic> findAll();

}
