package com.shoppertrak.store.dashboard.inventory;

import java.io.Serializable;

/**
 * @author Abhishek Shukla
 */
public class InventoryId implements Serializable {
	
	private static final long serialVersionUID = 7513615059638204916L;
	
	private String category;
	private String subCategory;
	private String model;

}
