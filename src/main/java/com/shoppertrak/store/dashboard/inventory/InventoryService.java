package com.shoppertrak.store.dashboard.inventory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppertrak.store.dashboard.repository.InventoryRepository;
/**
 * @author Abhishek Shukla
 */
@Service
public class InventoryService {
	
	private InventoryRepository repository;
	
	@Autowired
	public InventoryService(InventoryRepository repository) {
		this.repository = repository;
	}
	
	public List<Inventory> getInventoryDetails() {
		return repository.findAll();
	}

}

