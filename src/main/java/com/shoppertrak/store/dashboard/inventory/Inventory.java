package com.shoppertrak.store.dashboard.inventory;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author Abhishek Shukla
 */

@Entity
@Data
@Table(name = "inventory")
@IdClass(InventoryId.class)
public class Inventory implements Serializable {

	private static final long serialVersionUID = 7501236854841404284L;
	
	@NotNull
	@Column(name = "pos_date")
	private LocalDate posDate;
	
	@NotNull
	@Column(name = "brand_name")
	private String brandName;
	
	@Id
	@Column(name = "model_name")
	private String model;
	
	@Id
	@Column(name = "category")
	private String category;
	
	@Id
	@Column(name = "sub_category")
	private String subCategory;

}
